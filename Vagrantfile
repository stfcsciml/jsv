# -*- mode: ruby -*-
# vi: set ft=ruby :


# you're doing.
Vagrant.configure(2) do |config|

  #puts ENV['JUPYTER_PASSWORD']

  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # You can connect by:
  # ssh -i .vagrant/machines/default/virtualbox/private_key -p 2222 vagrant@127.0.0.1

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "bento/centos-7.4"
  config.vm.box_download_insecure=true
  # often use  config.vm.box = "centos/7"

  # Disable automatic box update checking. 
  # These configurations are fragile, 
  # Let's not invite the framework to break it.
  config.vm.box_check_update = false

  # This is the port for jupyter
  config.vm.network "forwarded_port", guest: 8888, host: 8888
  # This is the port for tensorboard
  config.vm.network "forwarded_port", guest: 6007, host: 6007
  
  # or create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "10.0.0.2"
  
  # Do not do this, since the jupyter installation is insecure:
  #config.vm.network "public_network"

  if Vagrant.has_plugin?("vagrant-vbguest")
      puts "Use of vbguest with this VM is not recommended."
      # vbguest succeeds only after kernel is updated
      config.vm.provision "shell", inline: "yum -y update kernel"
      config.vm.provision :reload
  end

  # pass any proxy details to guest
  if Vagrant.has_plugin?("vagrant-proxyconf")
    config.proxy.http     = ENV['http_proxy'] || ""
    config.proxy.https    = ENV['https_proxy'] || ""
    config.proxy.no_proxy = "localhost,127.0.0.1"
  end


  # Share an additional folder to the guest VM. 
  config.vm.synced_folder "./shared", "/vagrant", type: "virtualbox"

  config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  # Customize the amount of memory on the VM:
     vb.memory = "4096" # "12288" #"8192" #
     vb.linked_clone = true
  end  

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. 
  config.vm.provision :shell, path: "bootstrap.sh", args:[ ENV['JUPYTER_PASSWORD'] || 'password' ]
  
  # start jupyter
  config.trigger.after [:up, :reload, :resume], :stderr => true do
    #if Vagrant.has_plugin?("vagrant-vbguest")
      #run "vagrant vbguest --auto-reboot --no-provision"
      #run "mount -t vboxsf -o uid=1000,gid=1000 vagrant /vagrant"
    #end
    run_remote "sudo -u vagrant /usr/bin/singularity  instance.start --writable --bind /vagrant/shared:/shared /home/vagrant/sandbox/ jupyter"
  end

  # stop jupyter
  config.trigger.before [:halt, :suspend], :stderr => true do
    #if Vagrant.has_plugin?("vagrant-vbguest")
      #run "vagrant vbguest --auto-reboot --no-provision"
      #run "mount -t vboxsf -o uid=1000,gid=1000 vagrant /vagrant"
    #end
    run_remote "sudo -u vagrant /usr/bin/singularity instance.stop jupyter"
  end


end
