How to start this project in a VM
=================================

The directory structure is:

project:
    Vagrantfile
	bootstrap.sh
	shared: (mapped to Virtual Machine)
	    shared: (mounted in container)
		    environment.yml (list of python packages)
		    notebooks
	    
#Setting up vagrant-------------------------------------------------------

# To run these please ensure you have the following installed:
# Vagrant 1.9.6 (https://releases.hashicorp.com/vagrant/1.9.6/)
# VirtualBox 5.1.30 (https://www.virtualbox.org/wiki/Download_Old_Builds_5_1)
# Vagrant 2 and VirtualBox 5.2 have some defects.

#If you are behind an HTTP proxy:
export http_proxy=http://user:password@host:port
export https_proxy=https://user:password@host:port
vagrant plugin install vagrant-proxyconf


# For all users: plugins to install
vagrant plugin install vagrant-triggers
vagrant plugin uninstall vagrant-vbguest

#Creating the vagrant box ------------------------------------------------
JUPYTER_PASSWORD="your password" vagrant up
# this takes several minutes

#If there are any issues, please check the logs by:
vagrant ssh -c 'cat /var/log/vboxadd-install.log'

#After working, please stop the vagrant box session (don't forget to save any work!)
vagrant halt
#To start again after the vagrantbox has been created
vagrant up

# Visit  http://127.0.0.1:8888/notebooks/
