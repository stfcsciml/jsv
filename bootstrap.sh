JUPYTER_PASSWORD=${1:-password}

# installation for VirtualBox
yum install -y deltarpm 
yum install -y kernel-headers gcc 
# note that dkms, often recommended for virtualbox, is unsstable in Centos
# could mount iso and
# cd /media/VirtualBoxGuestAdditions
# ./VBoxLinuxAdditions.run

# requirements for rdkit.Chem
yum install -y libXrender libXext
# requirement for pydot
#yum install -y graphviz

yum install -y epel-release
#yum install -y openblas

# install singularity
#    yum update -y 
sudo yum install -y singularity-runtime singularity
singularity --version

# build singularity container with Anaconda in it
curl -L https://repo.anaconda.com/archive/Anaconda3-5.1.0-Linux-x86_64.sh -o /home/vagrant/conda.sh
# can build from recipe file only as root
singularity build conda.simg /vagrant/conda.spec
# now convert to a userspace sandbox
sudo -u vagrant singularity build --sandbox /home/vagrant/sandbox conda.simg
# ignore warnings, we will not run this container as root.
# also ignore error message, it is inaccurate

# configure jupyter and install dependencies:
mkdir sandbox/etc/jupyter
cat > sandbox/etc/jupyter/jupyter_notebook_config.py <<HERE0
c.NotebookApp.ip = '0.0.0.0'
c.NotebookApp.notebook_dir = '/shared/notebooks' 
c.NotebookApp.token = u'$JUPYTER_PASSWORD'
HERE0
sudo -u vagrant singularity exec --writable --bind /vagrant/shared:/shared sandbox/ \
	conda env update -f /tmp/environment.yml -n root

# if started with systemd, the instance seems to stop again immediately 
#cp  /vagrant/jupyter.service /usr/lib/systemd/system/jupyter.service
#chmod -x /usr/lib/systemd/system/jupyter.service
#systemctl daemon-reload
#systemctl enable jupyter
#systemctl start jupyter

echo 'Starting Jupyter. Please visit http://127.0.0.1:8888/notebooks/Welcome.ipynb'
# done as a trigger after up
#sudo -u vagrant /usr/bin/singularity  instance.start --writable --bind /vagrant/shared:/shared /home/vagrant/sandbox/ jupyter
