Bootstrap: yum
OSVersion: 7
MirrorURL: http://mirror.centos.org/centos-%{OSVERSION}/%{OSVERSION}/os/$basearch/
Include: yum deltarpm
# faster if edit config to bind host /var/cache/yum

%help

# Commands to execute on host system
%setup

# copy a config file into the container
%files
/home/vagrant/conda.sh /tmp/conda.sh

%labels
  Maintainer_Email chris.morris@stfc.ac.uk
  Anaconda_Version 2018.12
  # TODO was version 5.1.0
  
%environment
  export ANACONDA_VERSION=2018.12
  export PATH=/usr/local/anaconda/bin:$PATH

%post
  mkdir /shared
  export ANACONDA_VERSION=5.1.0
# TODO remove some of these
  yum -y install \
    bzip2 ca-certificates \
    libglib2.0-0 libxext6 libsm6 libxrender1 \
    git 
	# mercurial subversion vagrant 
  cd /usr/local
  #curl -L https://repo.anaconda.com/archive/Anaconda3-${ANACONDA_VERSION}-Linux-x86_64.sh -o Anaconda3-${ANACONDA_VERSION}-Linux-x86_64.sh
  #bash Anaconda3-${ANACONDA_VERSION}-Linux-x86_64.sh -b -p /usr/local/anaconda
  bash /tmp/conda.sh -b -p /usr/local/anaconda
  yum clean all
    
%apprun python
  exec python "${@}"

%apprun conda
  exec conda "${@}"

%runscript
  
%startscript
  jupyter notebook --no-browser

%test
